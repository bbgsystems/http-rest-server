package com.globalkinetic.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;

/**
 * Created by Prince Agbebiyi on 2017/08/16.
 *
 */
@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 180) //3 minutes
public class HttpSessionConfig extends AbstractHttpSessionApplicationInitializer {

    @Bean
    public JedisConnectionFactory connectionFactory() {
        return new JedisConnectionFactory();
    }



//    @Bean
//    public HttpSessionStrategy httpSessionStrategy() {
//        return new HttpSessionStrategy();
//    }
//    @Value( "${spring.datasource.url}" )
//    private String jdbcUrl;
//
//    @Value( "${spring.datasource.username}" )
//    private String username;
//
//    @Value( "${spring.datasource.driver-class-name}" )
//    private String jdbcDriver;
//    @Bean(name = "dataSource")
//    public DriverManagerDataSource dataSource() {
//        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
//        driverManagerDataSource.setDriverClassName(jdbcDriver);
//        driverManagerDataSource.setUrl(jdbcUrl);
//        driverManagerDataSource.setUsername(username);
//        driverManagerDataSource.setPassword("");
//        return driverManagerDataSource;
//    }

     

}