package com.globalkinetic.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.session.web.http.HttpSessionManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.globalkinetic.model.User;
import com.globalkinetic.model.ui.LoginRequest;
import com.globalkinetic.model.ui.LoginResponse;
import com.globalkinetic.model.ui.UserDto;
import com.globalkinetic.service.AuthenticationService;
import com.globalkinetic.service.UserService;


@RestController
@RequestMapping(value = "/api/")
@Produces("application/json")
public class UserController {

    private UserService userService;
    private AuthenticationService authenticationService;

    //private FindByIndexNameSessionRepository<? extends ExpiringSession> sessions;
    private static Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    public UserController (final UserService userService,
                           final AuthenticationService authenticationService) {
        this.userService = userService;
        this.authenticationService = authenticationService;
        //this.sessions = sessions;
    }

    @PutMapping(value = "/user/add")
    @HeaderParam("Accept=application/json")
    @ResponseBody
    public ResponseEntity add(@RequestBody User user) {
        logger.info("Received user {} ", user.getUsername());
        userService.add(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/users")
    @HeaderParam("Accept=application/json")
    @ResponseBody
    public ResponseEntity<List<UserDto>> users() {
        logger.info("Get all users");
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);

    }

    @PostMapping(value = "/user/login")
    @HeaderParam("Accept=application/json")
    @ResponseBody
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest request, @Context HttpServletRequest httpServletRequest ) {
        logger.info("login attempt for user {}", request.getUsername());
        HttpSession session = httpServletRequest.getSession(true);
        LoginResponse loginResponse = authenticationService.login(request, session.isNew());
        if(loginResponse !=null && loginResponse.getId() != null){ //success
            logger.info("User {} login success", request.getUsername());
            session.setAttribute("token", session.getId());
            session.setAttribute("id", loginResponse.getId());
            loginResponse.setToken(String.valueOf(session.getAttribute("token")));
            return new ResponseEntity<>(loginResponse, HttpStatus.OK);
        }
        logger.info("User {} login failed", request.getUsername());
        return new ResponseEntity<>( HttpStatus.UNAUTHORIZED);

    }

    @PostMapping(value = "/user/logout/{id}")
    @HeaderParam("Accept=application/json")
    @ResponseBody
    public ResponseEntity<String> logout(@PathVariable("id") Long id, @Context HttpServletRequest request) {
        HttpSessionManager sessionManager=(HttpSessionManager)request.getAttribute(HttpSessionManager.class.getName());
        Map<String, String> ids = sessionManager.getSessionIds(request);
        HttpSession session = request.getSession();
        Object sessionAttribute = session.getAttribute("id");
        if (session != null && id != null  && id.equals(Long.valueOf(String.valueOf(sessionAttribute)))){
            session.invalidate();
            String sessionId = session.getId();
            logger.info("User {} logout ok", id);
            return new ResponseEntity<>(sessionId, HttpStatus.OK);
        }
        logger.info("User {} logout failed", id);
        return new ResponseEntity<>( HttpStatus.BAD_REQUEST);

    }


    @GetMapping(value = "/user/recent")
    @HeaderParam("Accept=application/json")
    @ResponseBody
    public ResponseEntity<List<UserDto>> recentLogin(@RequestParam("interval") Long interval) {
        logger.info("Users who logged-in in the past {} minutes", interval);
        return new ResponseEntity<>(authenticationService.recentLogin(interval), HttpStatus.OK);

    }


}