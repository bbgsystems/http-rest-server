package com.globalkinetic.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.globalkinetic.model.SessionInfo;
import com.globalkinetic.repository.SessionInfoRepository;
import org.h2.engine.SessionRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.session.SessionRepository;
import org.springframework.session.web.http.HttpSessionManager;
import org.springframework.stereotype.Service;

import com.globalkinetic.model.User;
import com.globalkinetic.model.ui.LoginRequest;
import com.globalkinetic.model.ui.LoginResponse;
import com.globalkinetic.model.ui.UserDto;
import com.globalkinetic.repository.UserRepository;

/**
 * Created by Prince Agbebiyi on 2017/08/14.
 *
 */
@Service
@Transactional
public class UserServiceImpl implements  UserService {

    private UserRepository userRepository;
    private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    public UserServiceImpl(final UserRepository userRepository ) {
        this.userRepository = userRepository;
    }

    @Override
    public void add(User user) {
        logger.info("About to save user: {}", user.getUsername());
        User savedOrUpdate = null;
        //put is idempotent
        User exist =  userRepository.findByUsernameOrPhone(user.getUsername(), user.getPhone());
        if(exist != null){
            User update = new User();
            BeanUtils.copyProperties(user, update);
            update.setId(exist.getId());
            savedOrUpdate = update;
        } else {
            savedOrUpdate = user;
        }
        userRepository.save(savedOrUpdate);
        logger.info("Saved user: username: {} ID: {}", savedOrUpdate.getUsername(), savedOrUpdate.getId());
    }

    @Override
    public User getById(Long id) {
        logger.info("About to load user: {}", id);
        return userRepository.findOne(id);
    }

    @Override
    public List<UserDto> getUsers() {
        logger.info("Load users");
        List<User> users = userRepository.findAll();
        logger.info("Got total {} users", users.size());
        List<UserDto> usersResponse = new ArrayList<>();
        users.forEach(u-> usersResponse.add(u.toUserResponse()));
        return usersResponse;
    }
}
