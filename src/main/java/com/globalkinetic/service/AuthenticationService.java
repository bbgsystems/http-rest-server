package com.globalkinetic.service;

import com.globalkinetic.model.ui.LoginRequest;
import com.globalkinetic.model.ui.LoginResponse;
import com.globalkinetic.model.ui.UserDto;

import java.util.List;

/**
 * Created by Prince Agbebiyi on 2017/08/15.
 */
public interface AuthenticationService {
    LoginResponse login(LoginRequest request, boolean aNew);
    void logout(Long id);
    List<UserDto> recentLogin(Long interval);
}
