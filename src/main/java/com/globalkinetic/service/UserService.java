package com.globalkinetic.service;

import java.util.List;

import com.globalkinetic.model.User;
import com.globalkinetic.model.ui.LoginRequest;
import com.globalkinetic.model.ui.LoginResponse;
import com.globalkinetic.model.ui.UserDto;

/**
 * Created by Prince Agbebiyi on 2017/08/14.
 *
 */

public interface UserService {

    void add (User user);
    User getById(Long id);
    List<UserDto> getUsers();
    //LoginResponse login(LoginRequest request);
    //void logout(Long id);
    //List<UserDto>  recentLogin(Long interval);
}
