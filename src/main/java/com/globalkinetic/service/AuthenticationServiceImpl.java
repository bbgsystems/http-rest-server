package com.globalkinetic.service;

import com.globalkinetic.model.SessionInfo;
import com.globalkinetic.model.User;
import com.globalkinetic.model.ui.UserDto;
import com.globalkinetic.repository.SessionInfoRepository;
import com.globalkinetic.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.globalkinetic.model.ui.LoginRequest;
import com.globalkinetic.model.ui.LoginResponse;
import com.globalkinetic.repository.AuthenticationRepository;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Prince Agbebiyi on 2017/08/15.
 *
 */
@Service
public class AuthenticationServiceImpl implements  AuthenticationService {

    private static Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);
    private AuthenticationRepository authenticationRepository;
    private SessionInfoRepository sessionInfoRepository;

    private UserRepository userRepository;

    public AuthenticationServiceImpl(final AuthenticationRepository authenticationRepository,
                                     UserRepository userRepository, SessionInfoRepository sessionInfoRepository){
        this.authenticationRepository = authenticationRepository;
        this.userRepository = userRepository;
        this.sessionInfoRepository = sessionInfoRepository;
    }

    @Override
    public LoginResponse login(LoginRequest loginRequest, boolean isNew) {
        logger.info("About to login : {}", loginRequest.getUsername());
        User byUsernameAndPassword = userRepository.findByUsernameAndPassword(loginRequest.getUsername(), loginRequest.getPassword());
        if(byUsernameAndPassword == null){
            logger.info("No user found : {}", loginRequest.getUsername());
            return  null;
        }
        //update session info
        if(isNew || sessionInfoRepository.findByIdentifier(byUsernameAndPassword.getId()) == null){
            SessionInfo si = new SessionInfo();
            si.setActive(true);
            si.setTimestamp(new Date());
            si.setIdentifier(byUsernameAndPassword.getId());
            //sessionInfoRepository.save(si);

        }
        return byUsernameAndPassword.asReponse();

    }

    @Override
    public void logout(Long id) {
        logger.info("update session info for: {}", id);
        SessionInfo info =  sessionInfoRepository.findByIdentifier(id);
        info.setActive(false);
        sessionInfoRepository.save(info);
    }

    @Override
    public List<UserDto> recentLogin(Long interval) {
        if(interval ==null){
            interval = 5L;
        }
        long intervalAgoInMillis = System.currentTimeMillis() - TimeUnit.MILLISECONDS.toMillis(interval);
        List<SessionInfo> byCreatedBetween = sessionInfoRepository.findByTimestampBetween(intervalAgoInMillis, System.currentTimeMillis());
        return byCreatedBetween.stream().map( i-> new UserDto(i.getIdentifier() + "")).collect(Collectors.toList());

    }
}
