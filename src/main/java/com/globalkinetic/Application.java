package com.globalkinetic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = { "com.globalkinetic" })
@EntityScan("com.globalkinetic.model")
@ComponentScan("com.globalkinetic")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
