package com.globalkinetic.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.globalkinetic.model.SessionInfo;

import java.util.Date;
import java.util.List;

public interface SessionInfoRepository extends JpaRepository<SessionInfo, Long> {

    SessionInfo findByIdentifier(Long identifier);
    List<SessionInfo> findByTimestampBetween(Long now, Long interval);
}
