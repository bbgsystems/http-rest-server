package com.globalkinetic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.globalkinetic.model.User;

/**
 * Created by Prince Agbebiyi on 2017/08/14.
 *
 */
@Repository
public interface UserRepository  extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByUsernameOrPhone(String username, String phone);
    User findByUsernameAndPassword(String username, String password);

}
