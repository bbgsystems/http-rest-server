package com.globalkinetic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.globalkinetic.model.Login;

/**
 * Created by Prince Agbebiyi on 2017/08/14.
 *
 */
@Repository
public interface AuthenticationRepository extends JpaRepository<Login, Long> {

        Login findByUsernameAndPassword(String username, String password);
}
