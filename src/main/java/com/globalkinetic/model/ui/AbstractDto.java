package com.globalkinetic.model.ui;

/**
 * Created by Prince Agbebiyi on 2017/08/15.
 * Link: Prince.Agbebiyi@absa.co.za
 */
public class AbstractDto {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
