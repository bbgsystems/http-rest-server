package com.globalkinetic.model.ui;

/**
 * Created by Prince Agbebiyi on 2017
 */
public class UserDto extends AbstractDto {

    String phone;

    public UserDto(String id, String phone) {
        this.phone = phone;
        super.setId(id);
    }

    public UserDto(String id) {
        super.setId(id);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
