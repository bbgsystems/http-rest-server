package com.globalkinetic.model.ui;

/**
 * Created by Prince Agbebiyi on 2017/08/14.
 *
 */
public class LoginResponse extends AbstractDto {


    private String token;

    public LoginResponse() {

    }

    public LoginResponse(String id) {
        setId(id);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
