package com.globalkinetic.model;

import javax.persistence.Entity;

import com.globalkinetic.model.ui.LoginResponse;

/**
 * Created by Prince Agbebiyi on 2017
 */
@Entity
public class Login extends AbstractModel {


    public LoginResponse asReponse() {

        return new LoginResponse( String.valueOf(getId()));
    }
}
