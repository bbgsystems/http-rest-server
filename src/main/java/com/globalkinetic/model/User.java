package com.globalkinetic.model;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.globalkinetic.model.ui.LoginResponse;
import com.globalkinetic.model.ui.UserDto;

@Entity
public class User extends AbstractModel {

    private String phone;

    public User(String username, String password, String phone) {
        super.setUsername(username);
        super.setPassword(password);
        this.setPhone(phone);
    }

    public User() {
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    @Transient
    public UserDto toUserResponse() {
        return new UserDto(String.valueOf(getId()), getPhone());
    }


    @Transient
    public LoginResponse asReponse() {
        return new LoginResponse( String.valueOf(getId()));
    }
}
