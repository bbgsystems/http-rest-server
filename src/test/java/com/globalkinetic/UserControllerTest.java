package com.globalkinetic;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.globalkinetic.model.User;
import com.globalkinetic.model.ui.LoginRequest;
import com.globalkinetic.model.ui.LoginResponse;
import com.globalkinetic.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {


    @Autowired
    WebApplicationContext webCtx;

    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    UserRepository userRepository;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webCtx).build();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

    }

    @Test
    public void when_putValidUserRequest_Expect_Http201() throws Exception {
        byte[] userPayload = objectMapper.writeValueAsBytes(new User("tiger","1234", "07565656"));
        putUser(userPayload);

    }

    @Test
    public void when_getValidUserRequest_Expect_ListOfUsers() throws Exception {
         List<byte[]> userPayloads = Arrays.asList(
                 objectMapper.writeValueAsBytes(new User("tiger","1234", "07565656")),
                 objectMapper.writeValueAsBytes(new User("beth","111", "0223232323")));
        userPayloads.forEach(userPayload-> {
            try {
                putUser(userPayload);
            } catch (Exception e) {
                e.printStackTrace();
            }});
        List<User> users = objectMapper.readValue(getUsers(), new TypeReference<List<User>>() {});
        assertThat(users).isNotNull();
        assertThat(users).isNotEmpty();

    }


    @Test
    public void when_postValidLoginRequest_Expect_LoginResponse() throws Exception {
        //Create user
        String username = "tiger";
        String password = "1234";
        String phone = "07565656";
        byte[] userPayload = objectMapper.writeValueAsBytes(new User(username, password, phone));
        putUser(userPayload);

        //login user
        byte[] loginPayload = objectMapper.writeValueAsBytes(new LoginRequest(username, password));
        MvcResult result = mockMvc.perform(post("/api/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(loginPayload)).andReturn();
        //check response
        LoginResponse loginResponse = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<LoginResponse>() {});
        assertThat(loginResponse).isNotNull();
        assertThat(loginResponse.getId()).isNotNull();
        assertThat(loginResponse.getToken()).isNotNull();
        User user = userRepository.findOne(Long.valueOf(loginResponse.getId()));
        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo(username);
        assertThat(user.getPhone()).isEqualTo(phone);

    }

    // utils
    private ResultActions putUser(byte[] userPayload) throws Exception {
        return mockMvc.perform(put("/api/user/add")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(userPayload))
                .andExpect(status().isCreated());
    }


    private String getUsers() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        return result.getResponse().getContentAsString();
    }
}
